#ifndef BACHELOR_ECMSMOOTHCHECKER_H
#define BACHELOR_ECMSMOOTHCHECKER_H

#include "../common.h"
#include "../Interfaces/ISmoothChecker.h"

class ECMSmoothChecker : public ISmoothChecker
  {
 public:
  ECMSmoothChecker(const ZZ& i_bnd);

  virtual bool IsSmooth(Vec<ZZ_p>& o_quotients, const Vec<ZZ_p>& i_factors,
                        const ZZ_p& i_integer) const override;

 private:
  ZZ m_bnd;
  };

#endif //BACHELOR_ECMSMOOTHCHECKER_H
