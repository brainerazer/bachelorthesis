#ifndef BACHELOR_ADLEMANSIEVE_H
#define BACHELOR_ADLEMANSIEVE_H

#include "../Interfaces/ISieve.h"
#include "../Interfaces/ISmoothChecker.h"

class AdlemanSieve : public ISieve
  {
 public:
  AdlemanSieve(const ISmoothChecker& i_checker);

  virtual void SetSmallFactors(const Vec<ZZ_p> &i_small_factors) override;
  virtual void SetGenerator(const ZZ_p &i_generator) override ;

  virtual void GetLinearSystem(Mat<ZZ_p> &o_A, Vec<ZZ_p> &o_b, size_t i_size) override;

 private:
  Vec<ZZ_p> _GetSingleRelation(ZZ_p &o_u);

 private:
  Vec<ZZ_p> m_small_factors;
  const ISmoothChecker& m_checker;
  ZZ_p m_generator;
  };

#endif //BACHELOR_ADLEMANSIEVE_H
