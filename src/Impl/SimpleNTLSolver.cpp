#include "SimpleNTLSolver.h"

#include <NTL/mat_ZZ_p.h>

SimpleNTLSolver::SimpleNTLSolver()
  {
  }

bool SimpleNTLSolver::SolveSystem(Vec<ZZ_p> &o_solution, const Mat<ZZ_p> &i_A, const Vec<ZZ_p> &i_b, const ZZ &i_modulus)
  {
  ZZ_pPush push_(i_modulus);

  ZZ_p d;
  solve(d, o_solution, transpose(i_A), i_b);

  if(d != ZZ_p(0))
    return true;
  else
    return false;
  }