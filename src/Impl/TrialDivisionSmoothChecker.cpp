#include "TrialDivisionSmoothChecker.h"

TrialDivisionSmoothChecker::TrialDivisionSmoothChecker()
  {
  }

bool TrialDivisionSmoothChecker::IsSmooth(Vec<ZZ_p> &o_quotients, const Vec<ZZ_p> &i_factors,
                                          const ZZ_p &i_integer) const
  {
  ZZ num = rep(i_integer);
  Vec<ZZ_p> result(INIT_SIZE, i_factors.length());
  ZZ q, r;
  for(size_t i = 0; i < i_factors.length(); i++)
    {
    long counter = 0;
    r = ZZ::zero();
    while (r == ZZ::zero())
      {
      DivRem(q, r, num, rep(i_factors[i]));
      if(r == ZZ::zero())
        {
        counter++;
        num = q;
        }
      }

    result[i] = counter;
    }

  o_quotients = result;
  if(num == ZZ(1))
    return true;
  else
    return false;
  }