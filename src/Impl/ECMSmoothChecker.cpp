#include <mpi.h>
#include <boost/mpi/communicator.hpp>
#include "ECMSmoothChecker.h"

#include "../ZZFactoring.h"
#include <boost/mpi.hpp>

ECMSmoothChecker::ECMSmoothChecker(const ZZ &i_bnd)
  : m_bnd(i_bnd)
  {
  }

bool ECMSmoothChecker::IsSmooth(Vec<ZZ_p> &o_quotients, const Vec<ZZ_p> &i_factors,
                                          const ZZ_p &i_integer) const
  {
  ZZ num = rep(i_integer);

  o_quotients = Vec<ZZ_p>(INIT_SIZE, i_factors.length(), ZZ_p::zero());

  if(num == ZZ(1))
    return true;


//  static int tries = 0;
//  tries++;
  Vec<Pair<ZZ, long>> out;
  factor(out, num, m_bnd, 0.1);

  if(out.length() == 0)
    return false;

//  std::cout << "m_bnd " << m_bnd << " out " << out << std::endl;
//  std::cout << "ifa " << i_factors << " len " << i_factors.length() << std::endl;

  long pos = 0;
  for(auto q : out)
    {
//    std::cout << "num " << num << " q.a " << q.a << " q.b " << q.b << std::endl;
    ZZ_p a = to_ZZ_p(q.a);
    while(i_factors[pos] != a)
      {
      pos++;
//      std::cout << "a " << a << " pos " << pos << std::endl;
      if (pos >= i_factors.length())
        return false;
      }

    for(long i = 0; i < q.b; i++)
      div(num, num, q.a);

    //std::cout << "zzpqa " << a << " pos " << pos << std::endl;

    if(pos != -1)
      o_quotients[pos] = to_ZZ_p(q.b);
    }

//  std::cout << "wuuh num " << num << " quo " << o_quotients <<  std::endl;

  if(num == 1)
    {
//    std::cout << "worker " << boost::mpi::communicator().rank() << " needed " << tries << std::endl;
//    tries = 0;
    return true;
    }
  else
    return false;
  }