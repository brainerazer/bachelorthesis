#ifndef BACHELOR_NONDETERMLANCZOSSOLVER_H
#define BACHELOR_NONDETERMLANCZOSSOLVER_H

#include "../common.h"
#include "../Interfaces/ILinearSolver.h"

class NondetermLanczosSolver : public ILinearSolver
  {
 public:
  NondetermLanczosSolver();

  virtual bool SolveSystem(Vec<ZZ_p> &o_solution, const Mat<ZZ_p> &i_A, const Vec<ZZ_p> &i_b, const ZZ &i_modulus) override;
  };

#endif //BACHELOR_NONDETERMLANCZOSSOLVER_H
