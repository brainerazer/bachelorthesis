#include "AdlemanSieve.h"

AdlemanSieve::AdlemanSieve(const ISmoothChecker& i_checker)
  : m_checker(i_checker)
  {
  }

void AdlemanSieve::SetSmallFactors(const Vec<ZZ_p> &i_small_factors)
  {
  m_small_factors = i_small_factors;
  }

void AdlemanSieve::SetGenerator(const ZZ_p &i_generator)
  {
  m_generator = i_generator;
  }

void AdlemanSieve::GetLinearSystem(Mat<ZZ_p> &o_A, Vec<ZZ_p> &o_b, size_t i_size)
  {
  long n = 0;
  long m = m_small_factors.length();
  Mat<ZZ_p> matrix;
  Vec<ZZ_p> b;
  matrix.SetDims(n, m);
  ZZ_p r;
  while(n < i_size)
    {
    auto row = _GetSingleRelation(r);
    if(n % 10 == 0)
      std::cout << "Relations: " << n + 1 << " out of " << i_size << std::endl;
    n++;
    matrix.SetDims(n, m);
    matrix(n) = row;
    b.append(r);
    }

  o_A = matrix;
  o_b = b;
  }

Vec<ZZ_p> AdlemanSieve::_GetSingleRelation(ZZ_p &o_u)
  {
  bool found_smooth = false;
  ZZ_p u, r;
  ZZ_p one(1);
  Vec<ZZ_p> quotients;
  while(!found_smooth)
    {
    random(u);
    power(r, m_generator, rep(u));

    //std::cout << "g, u, R: " << m_generator << ' ' << u << ' ' << ' ' << r << std::endl;
    if(r != ZZ_p::zero() && r != one)
      found_smooth = m_checker.IsSmooth(quotients, m_small_factors, r);
    }

//  std::cout << "num " << r << " quo " << quotients << std::endl;
  o_u = u;
  return quotients;
  }