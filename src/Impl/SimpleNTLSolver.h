#ifndef BACHELOR_SIMPLENTLSOLVER_H
#define BACHELOR_SIMPLENTLSOLVER_H

#include "../common.h"
#include "../Interfaces/ILinearSolver.h"

class SimpleNTLSolver : public ILinearSolver
  {
 public:
  SimpleNTLSolver();

  // Accepts only square matrice A :(
  virtual bool SolveSystem(Vec<ZZ_p> &o_solution, const Mat<ZZ_p> &i_A, const Vec<ZZ_p> &i_b, const ZZ &i_modulus) override;
  };

#endif //BACHELOR_SIMPLENTLSOLVER_H
