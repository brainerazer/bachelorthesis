#ifndef BACHELOR_TRIALDIVISIONSMOOTHCHECKER_H
#define BACHELOR_TRIALDIVISIONSMOOTHCHECKER_H

#include "../common.h"
#include "../Interfaces/ISmoothChecker.h"

class TrialDivisionSmoothChecker : public ISmoothChecker
  {
 public:
  TrialDivisionSmoothChecker();

  virtual bool IsSmooth(Vec<ZZ_p>& o_quotients, const Vec<ZZ_p>& i_factors,
                        const ZZ_p& i_integer) const override;
  };

#endif //BACHELOR_TRIALDIVISIONSMOOTHCHECKER_H
