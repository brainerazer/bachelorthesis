#ifndef BACHELOR_APPLICATION_H
#define BACHELOR_APPLICATION_H

#include <boost/mpi/communicator.hpp>
#include "common.h"

class Application
  {
 public:
  Application(int argc, char* argv[]);

  int Run();

 private:
  void _GenerateModulus();
  void _InitGenerator();
  void _InitFactorBase();

  void _GetLinearSystem(Mat<ZZ_p> &o_A, Vec<ZZ_p> &o_b, const Vec<ZZ_p> &i_factor_base,
                        const ZZ_p &i_generator, long i_size);

  void _FindRelations(Mat<ZZ_p> &o_A, Vec<ZZ_p> &o_b);
  void _SolveSystemAndGetLogs(Vec<ZZ_p> &o_x, const Mat<ZZ_p> &i_A, const Vec<ZZ_p> &i_b);

  ZZ_p _FindLog(const ZZ_p &i_unknown);

  void _QuitAllButMaster();

 private:
  long        m_bits;
  long        m_bound;
  ZZ          m_modulus;
  ZZ_p        m_generator;
  Vec<ZZ_p>   m_factor_base;

  Vec<ZZ_p>   m_factor_base_logs;

  boost::mpi::communicator m_communicator;
  };

#endif //BACHELOR_APPLICATION_H
