#include "primes.h"

std::vector<llint> get_primes(llint bound)
  {
  std::vector<bool> is_prime(bound, true);
  std::vector<llint> primes;

  is_prime[0] = is_prime[1] = false;
  for(llint i = 2; i < bound; i++)
    {
    if(is_prime[i])
      {
      primes.push_back(i);
      for(llint j = i * 2; j < bound; j = j + i)
        is_prime[j] = false;
      }
    }

  return primes;
  }

