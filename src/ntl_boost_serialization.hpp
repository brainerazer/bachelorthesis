#ifndef BACHELOR_NTL_BOOST_SERIALIZATION_HPP
#define BACHELOR_NTL_BOOST_SERIALIZATION_HPP

#include <NTL/ZZ_p.h>
#include <NTL/matrix.h>
#include <NTL/vec_ZZ_p.h>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/split_free.hpp>
#include <boost/lexical_cast.hpp>

using namespace NTL;

namespace boost
  {
    namespace serialization
      {
        template <typename Archive>
        void save(Archive& ar, NTL::ZZ const& v, unsigned int const) {
          std::string str(boost::lexical_cast<std::string>(v));
          ar
          & boost::serialization::make_nvp("str", str)
                  ;
          }
        template <typename Archive>
        void load(Archive& ar, NTL::ZZ& v, unsigned int const) {
          std::string str;
          ar
          & boost::serialization::make_nvp("str", str)
                  ;
          v = NTL::to_ZZ(str.c_str());
          }

        template <typename Archive>
        void serialize(Archive& ar, NTL::ZZ& v, unsigned int const version) {
          boost::serialization::split_free(ar, v, version);
          }

        //-----------------------------------------------------------------------------

        template <typename Archive>
        void save(Archive& ar, NTL::ZZ_p const& v, unsigned int const) {
          std::string str(boost::lexical_cast<std::string>(v));
          ar
          & boost::serialization::make_nvp("str", str)
                  ;
          }
        template <typename Archive>
        void load(Archive& ar, NTL::ZZ_p& v, unsigned int const) {
          std::string str;
          ar
          & boost::serialization::make_nvp("str", str)
                  ;
          v = NTL::to_ZZ_p(conv<ZZ>(str.c_str()));
          }
        template <typename Archive>
        void serialize(Archive& ar, NTL::ZZ_p& v, unsigned int const version) {
          boost::serialization::split_free(ar, v, version);
          }

        //-----------------------------------------------------------------------------
        template <typename Archive>
        void save(Archive& ar, Vec<ZZ_p> const& v, unsigned int const) {
          ar & v.length();

          for(auto elem : v)
            {
            ar & elem;
            }
          }
        template <typename Archive>
        void load(Archive& ar, Vec<ZZ_p>& v, unsigned int const) {
          long n;
          ar & n;

          v = Vec<ZZ_p>(NTL::INIT_SIZE, n);
          for(long i = 0; i < n; i++) {
            ar & v[i];
            }
          }
        template <typename Archive>
        void serialize(Archive& ar, Vec<ZZ_p>& v, unsigned int const version) {
          boost::serialization::split_free(ar, v, version);
          }

        //-----------------------------------------------------------------------------
        template <typename Archive>
        void save(Archive& ar, Mat<ZZ_p> const& v, unsigned int const) {
          long rows = v.NumRows();
          long cols = v.NumCols();

          ar & rows;
          ar & cols;

          for(size_t i = 0; i < rows; i++)
            {
            ar & v[i];
            }
          }
        template <typename Archive>
        void load(Archive& ar, Mat<ZZ_p>& v, unsigned int const) {
          long rows, cols;
          ar & rows;
          ar & cols;

          v = Mat<ZZ_p>(NTL::INIT_SIZE, rows, cols);
          for(long i = 0; i < rows; i++) {
            ar & v[i];
            }
          }
        template <typename Archive>
        void serialize(Archive& ar, Mat<ZZ_p>& v, unsigned int const version) {
          boost::serialization::split_free(ar, v, version);
          }

      }
  }

#endif //BACHELOR_NTL_BOOST_SERIALIZATION_HPP
