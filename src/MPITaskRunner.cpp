#include "MPITaskRunner.h"

#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>

MPITaskRunner::MPITaskRunner(boost::mpi::communicator& i_communicator)
  : m_communicator(i_communicator)
  {
  _Reset();
  }

void MPITaskRunner::SetTask(MPITaskType i_type, std::function<void()> i_task)
  {
  switch(i_type)
    {
    case MPITaskType::MASTER:
      m_master_functor = i_task;
      break;
    case MPITaskType::SLAVES:
      m_slaves_functor = i_task;
      break;
    case MPITaskType::ALL_WORKERS:
      m_master_functor = i_task;
      m_slaves_functor = i_task;
      break;

    default:
      throw std::logic_error("Strange input argument to MPITaskRunner::SetTask");
    }

  }

void MPITaskRunner::_Reset()
  {
  m_master_functor = nullptr;
  m_slaves_functor = nullptr;
  }

void MPITaskRunner::Run()
  {
  if(m_communicator.rank() == 0)
    {
    if (m_master_functor)
      m_master_functor();
    }
  else
    {
    if (m_slaves_functor)
      m_slaves_functor();
    }

  _Reset();
  }

void MPITaskRunner::SetTaskAndRun(MPITaskType i_type, std::function<void()> i_task)
  {
  SetTask(i_type, i_task);
  Run();
  }









