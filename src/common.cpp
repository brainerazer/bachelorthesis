#include "common.h"

Vec<ZZ_p> convert_vector(const std::vector<llint>& i_vec)
  {
  Vec<ZZ_p> result;
  for(auto p : i_vec)
    result.append(ZZ_p(p));

  return result;
  }
