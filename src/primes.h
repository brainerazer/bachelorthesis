#ifndef BACHELOR_PRIMES_H
#define BACHELOR_PRIMES_H

#include <vector>
#include "common.h"

// Gets primes less than bound starting with 2
// using Eratosthenes sieve (TODO change to Atkin if critical)
std::vector<llint> get_primes(llint bound);

#endif //BACHELOR_PRIMES_H
