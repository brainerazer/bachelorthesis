#ifndef BACHELOR_ISMOOTHCHECKER_H
#define BACHELOR_ISMOOTHCHECKER_H

#include "../common.h"

struct ISmoothChecker
  {
  // Checks ZZ_p for smoothness in factor base. Return a vector of quotients if succesful.
  virtual bool IsSmooth(Vec<ZZ_p>& o_quotients, const Vec<ZZ_p>& i_factors,
                        const ZZ_p& i_integer) const = 0;
  };

#endif //BACHELOR_ISMOOTHCHECKER_H
