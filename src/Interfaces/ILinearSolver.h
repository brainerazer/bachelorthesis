#ifndef BACHELOR_ISOLVER_H
#define BACHELOR_ISOLVER_H

#include "../common.h"

struct ILinearSolver
  {
  // Solves system in the form of A * x = b
  virtual bool SolveSystem(Vec<ZZ_p> &o_solution, const Mat<ZZ_p> &i_A, const Vec<ZZ_p> &i_b,
                           const ZZ &i_modulus) = 0;
  };

#endif //BACHELOR_ISOLVER_H
