#ifndef BACHELOR_ISIEVE_H
#define BACHELOR_ISIEVE_H

#include "../common.h"

struct ISieve
  {
  virtual void SetSmallFactors(const Vec<ZZ_p> &i_small_factors) = 0;
  virtual void SetGenerator(const ZZ_p &i_generator) = 0;

  // In the form of A * x = b, where x is small factors vector
  virtual void GetLinearSystem(Mat<ZZ_p> &o_A, Vec<ZZ_p> &o_b, size_t i_size) = 0;
  };

#endif //BACHELOR_ISIEVE_H
