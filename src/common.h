#ifndef BACHELOR_COMMON_H
#define BACHELOR_COMMON_H

#include <iostream>
#include <vector>
#include <NTL/ZZ_p.h>
#include <NTL/matrix.h>
#include <NTL/vec_ZZ_p.h>

using namespace NTL;

//typedef unsigned long long int llint;
typedef long int llint;

Vec<ZZ_p> convert_vector(const std::vector<llint>& i_vec);

inline double L_p(const ZZ& p, double c) {
  double lm = NTL::log(p);
  double llm = ::log(lm);
  return exp(c*sqrt(lm*llm));
  }


#endif //BACHELOR_COMMON_H
