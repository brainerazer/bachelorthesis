#include "Application.h"

#include "primes.h"
#include "common.h"
#include "time.h"
#include "ntl_boost_serialization.hpp"
#include "Impl/TrialDivisionSmoothChecker.h"
#include "Impl/AdlemanSieve.h"
#include "Impl/NondetermLanczosSolver.h"
#include "MPITaskRunner.h"
#include "Impl/ECMSmoothChecker.h"
#include <NTL/pair.h>

#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/mpi/collectives.hpp>

namespace mpi = boost::mpi;
using namespace std;

// test if g is a generator of the group ZZ_p
// f is the factorization of p-1
bool isGenerator(const ZZ_p& g, const Vec<Pair<ZZ, long>>& f) {
  ZZ m,e;
  ZZ_p t;
  sub(m,ZZ_p::modulus(),1);
  for (long i=0; i<f.length(); ++i) {
    div(e,m,f[i].a);
    power(t,g,e);
    if (IsOne(t))
      return false;
    }
  return true;
  }

void Application::_GetLinearSystem(Mat<ZZ_p> &o_A, Vec<ZZ_p> &o_b, const Vec<ZZ_p> &i_factor_base,
                     const ZZ_p &i_generator, long i_size)
  {
  TrialDivisionSmoothChecker smooth_checker;
  ECMSmoothChecker ecm_checker(to_ZZ(m_bound));
  AdlemanSieve sieve(ecm_checker);

  sieve.SetSmallFactors(i_factor_base);
  sieve.SetGenerator(i_generator);

  sieve.GetLinearSystem(o_A, o_b, i_size);
  }

// Assuming global modulus is set and the other factor is 2
ZZ_p GetLog(const ZZ_p &i_el, const ZZ_p &i_solution, const ZZ &i_solution_factor,
            const ZZ_p &i_generator)
  {
  if(i_solution == ZZ_p::zero())
    return i_solution;
  if(power(i_generator, rep(i_solution)) == i_el)
    return i_solution;
  else
    return i_solution + to_ZZ_p(i_solution_factor);
  }

Application::Application(int argc, char **argv)
  {
  m_communicator = boost::mpi::communicator(); // default a.k.a world
  if (argc < 2)
    m_bits = 40;
  else
    m_bits = std::stol(std::string(argv[1]));

  if(argc >= 3)
    m_modulus = to_ZZ(argv[2]);
  else
    m_modulus = 0;

  if(argc >= 4)
    m_bound = std::atol(argv[3]);
  else
    m_bound = 0;
  }

void Application::_GenerateModulus()
  {
  MPITaskRunner runner(m_communicator);

  if(m_modulus == ZZ::zero())
    runner.SetTaskAndRun(MPITaskType::MASTER, [this]() {
        ZZ prim = GenGermainPrime_ZZ(m_bits);
       m_modulus = prim * 2 + 1;
    });

  mpi::broadcast(m_communicator, m_modulus, 0);
  ZZ_p::init(m_modulus);
  }

void Application::_InitGenerator()
  {
  MPITaskRunner runner(m_communicator);

  runner.SetTaskAndRun(MPITaskType::MASTER, [this]() {
      Vec<Pair<ZZ, long>> f;
      f.SetLength(2);
      f[0].a = 2;
      f[0].b = 1;
      f[1].a = (m_modulus - 1) / 2;
      f[1].b = 1;

      do
        {
        random(m_generator);
        } while (!isGenerator(m_generator, f));
  });

  mpi::broadcast(m_communicator, m_generator, 0);
  }

void Application::_InitFactorBase()
  {
  MPITaskRunner runner(m_communicator);

  runner.SetTaskAndRun(MPITaskType::MASTER, [this]() {
    if(m_bound == 0)
      m_bound = (long)(2*L_p(m_modulus, 1/sqrt(2)));
    std::cout << "Bound: " << m_bound << std::endl;
      m_factor_base = convert_vector(get_primes(m_bound));
  });

  mpi::broadcast(m_communicator, m_bound, 0);
  mpi::broadcast(m_communicator, m_factor_base, 0);
  }

void Application::_FindRelations(Mat<ZZ_p> &o_A, Vec<ZZ_p> &o_b)
  {
  m_communicator.barrier();
  auto start = MPI_Wtime();

  MPITaskRunner runner(m_communicator);

  long total_size = m_factor_base.length() * 4; // should be divisible by workers number
  long per_worker = total_size / m_communicator.size();

  Mat<ZZ_p> A;
  Vec<ZZ_p> b;

  runner.SetTaskAndRun(MPITaskType::ALL_WORKERS, [this, &A, &b, per_worker](){
    _GetLinearSystem(A, b, m_factor_base, m_generator, per_worker);
  });

  m_communicator.barrier();

  std::vector<Mat<ZZ_p>> all_matrices;
  std::vector<Vec<ZZ_p>> all_vectors;

  runner.SetTask(MPITaskType::MASTER, [this, &A, &all_matrices]() {
    mpi::gather(m_communicator, A, all_matrices, 0);
  });

  runner.SetTask(MPITaskType::SLAVES, [this, &A]() {
    mpi::gather(m_communicator, A, 0);
  });

  runner.Run();

  runner.SetTask(MPITaskType::MASTER, [this, &b, &all_vectors]() {
    mpi::gather(m_communicator, b, all_vectors, 0);
  });

  runner.SetTask(MPITaskType::SLAVES, [this, &b]() {
    mpi::gather(m_communicator, b, 0);
  });

  runner.Run();  // TODO investigate if all this could be put in a single Run() call

  runner.SetTaskAndRun(MPITaskType::MASTER, [this, &o_A, &o_b, &all_matrices, &all_vectors, total_size]() {
    o_A.SetDims(total_size, m_factor_base.length());
    o_b.SetLength(total_size);

    long cnt = 0;
    for(int i = 0; i < all_matrices.size(); i++)
      for(int j = 0; j < all_matrices[i].NumRows(); j++)
        {
        o_A[cnt] = all_matrices[i][j];
        cnt++;
        }

    cnt = 0;
    for(int i = 0; i < all_vectors.size(); i++)
      for(int j = 0; j < all_vectors[i].length(); j++)
        {
        o_b[cnt] = all_vectors[i][j];
        cnt++;
        }
  });

  m_communicator.barrier();
  auto end = MPI_Wtime();

  std::cout << "Time finding relations: " << end - start << std::endl;
  }

void Application::_QuitAllButMaster()
  {
  MPITaskRunner runner(m_communicator);
  runner.SetTaskAndRun(MPITaskType::SLAVES, [] () {
    MPI_Finalize();
    exit(0);
  });
  }

void Application::_SolveSystemAndGetLogs(Vec<ZZ_p> &o_x, const Mat<ZZ_p> &i_A, const Vec<ZZ_p> &i_b)
  {
  auto start = MPI_Wtime();

  ZZ q = (m_modulus - 1) / 2;
  NondetermLanczosSolver solver;

  if (!solver.SolveSystem(o_x, i_A, i_b, q))
    {
    std::cout << "FAIL" << std::endl;
    return;
    }

  // Now we get logs of factor base
  for(size_t i = 0; i < o_x.length(); i++)
    o_x[i] = GetLog(m_factor_base[i], o_x[i], q, m_generator);

  auto end = MPI_Wtime();
  std::cout << "Time solving system: " << end - start << std::endl;
  }

ZZ_p Application::_FindLog(const ZZ_p &i_unknown)
  {
  auto start = MPI_Wtime();

  ZZ_p output;

  TrialDivisionSmoothChecker smooth_checker;
  bool found_smooth = false;
  ZZ_p u, r;
  ZZ_p one(1);
  Vec<ZZ_p> quotients;
  while(!found_smooth)
    {
    u = ZZ_p(random_ZZ_p());
    r = i_unknown * power(m_generator, rep(u));

    if(r != ZZ_p::zero() && r != one)
      found_smooth = smooth_checker.IsSmooth(quotients, m_factor_base, r);
    }

  ZZ_pPush push_(m_modulus - 1);

  for (size_t i = 0; i < m_factor_base_logs.length(); i++)
    output += quotients[i] * m_factor_base_logs[i];

  output -= u;

  auto end = MPI_Wtime();
  std::cout << "Time finding log: " << end - start << std::endl;

  return output;
  }

int Application::Run()
  {
  /* Global initialization */
  mpi::environment env;
  mpi::communicator world;
  std::cout << "I am process " << world.rank() << " of " << world.size()
  << "." << std::endl;
  NTL::SetSeed(conv<ZZ>((long) time(NULL)));
  /* --------------------- */

  _GenerateModulus();

  _InitGenerator();
  _InitFactorBase();

  Mat<ZZ_p> A;
  Vec<ZZ_p> b;

  // Highly parallelizable step
  _FindRelations(A, b);
  _QuitAllButMaster(); // No need to have other workers anymore

  _SolveSystemAndGetLogs(m_factor_base_logs, A, b);

  // No input is provided, so generate.
  // TODO input!
  ZZ_p y = random_ZZ_p();
  ZZ_p log_y = _FindLog(y);

  if(log_y != ZZ_p::zero())
    std::cout << "Log of " << y << " mod " << m_modulus  << " gen " << m_generator
    << " is " << log_y << std::endl;

  bool check = (power(m_generator, rep(log_y)) == y);
  std::cout << "Check: " << std::boolalpha << check << std::endl;
  }






