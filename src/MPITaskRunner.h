#ifndef BACHELOR_MPITASKRUNNER_H
#define BACHELOR_MPITASKRUNNER_H

#include <functional>
#include <boost/mpi/communicator.hpp>

enum class MPITaskType
  {
  MASTER,
  SLAVES,
  ALL_WORKERS
  };

class MPITaskRunner
  {
 public:
  MPITaskRunner(boost::mpi::communicator& i_comminicator);

  void SetTask(MPITaskType i_type, std::function<void()> i_task);
  void SetTaskAndRun(MPITaskType i_type, std::function<void()> i_task);

  void Run();

 private:
  void _Reset();

 private:
  std::function<void()> m_master_functor;
  std::function<void()> m_slaves_functor;

  boost::mpi::communicator& m_communicator;
  };

#endif //BACHELOR_MPITASKRUNNER_H
