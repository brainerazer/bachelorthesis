#include "../src/common.h"
#include "../src/primes.h"

#include "catch.hpp"

TEST_CASE("Primes are generated", "[primes]")
  {
  SECTION("Primes up to 100 are generated")
    {
    std::vector<llint> reference = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29,
                                    31, 37, 41, 43, 47, 53, 59, 61, 67,
                                    71, 73, 79, 83, 89, 97};

    std::vector<llint> primes = get_primes(100);

    REQUIRE(reference == primes);
    }
  }