#include "catch.hpp"
#include "../src/common.h"
#include "../src/Impl/SimpleNTLSolver.h"

TEST_CASE("NTL solver works somehow", "[smooth]")
  {
  SimpleNTLSolver solver;
  ZZ_p::init(ZZ(83));

  Vec<ZZ_p> factor_base;
  factor_base.append(ZZ_p(2));
  factor_base.append(ZZ_p(3));
  factor_base.append(ZZ_p(5));
  factor_base.append(ZZ_p(7));

  Vec<ZZ_p> quotients;

  SECTION("Solves predefined system")
    {
    Mat<ZZ_p> A;
    Vec<ZZ_p> b;

    A.SetDims(4, 4);
    A[0][0] = 1;
    A[0][1] = 1;
    A[0][2] = 1;
    A[0][3] = 0;

    A[1][0] = 0;
    A[1][1] = 0;
    A[1][2] = 1;
    A[1][3] = 1;

    A[2][0] = 2;
    A[2][1] = 1;
    A[2][2] = 1;
    A[2][3] = 0;

    A[3][0] = 0;
    A[3][1] = 0;
    A[3][2] = 1;
    A[3][3] = 0;

    b.append(ZZ_p(18));
    b.append(ZZ_p(35));
    b.append(ZZ_p(19));
    b.append(ZZ_p(27));

    SimpleNTLSolver solver;
    Vec<ZZ_p> result;
    solver.SolveSystem(result, A, b, ZZ(82));

    Vec<ZZ_p> truth;
    truth.append(ZZ_p(1));
    truth.append(ZZ_p(72));
    truth.append(ZZ_p(27));
    truth.append(ZZ_p(8));

    REQUIRE(truth == result);
    }


  }