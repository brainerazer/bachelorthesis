#include "catch.hpp"
#include "../src/common.h"
#include "../src/Impl/TrialDivisionSmoothChecker.h"
#include "../src/Impl/ECMSmoothChecker.h"
#include "../src/primes.h"


TEST_CASE("Trial division smooth checker works good", "[smooth]")
  {
  //TrialDivisionSmoothChecker checker;
  ECMSmoothChecker checker(to_ZZ(9));
  ZZ_p::init(ZZ(83));

  Vec<ZZ_p> factor_base;
  factor_base.append(ZZ_p(2));
  factor_base.append(ZZ_p(3));
  factor_base.append(ZZ_p(5));
  factor_base.append(ZZ_p(7));

  Vec<ZZ_p> quotients;

  SECTION("1 is smooth and factors are 0")
    {
    Vec<ZZ_p> truth;
    truth.SetLength(4, ZZ_p::zero());
    bool res = checker.IsSmooth(quotients, factor_base, ZZ_p(1));

    REQUIRE(true == res);
    REQUIRE(truth == quotients);
    }

  SECTION("45 is smooth and factors good")
    {
    // 45 = 3 * 3 * 5
    Vec<ZZ_p> truth;
    truth.SetLength(4, ZZ_p::zero());
    truth[0] = 0;
    truth[1] = 2;
    truth[2] = 1;
    truth[3] = 0;

    bool res = checker.IsSmooth(quotients, factor_base, ZZ_p(45));

    REQUIRE(true == res);
    REQUIRE(truth == quotients);
    }

  SECTION("31 is not smooth")
    {
    bool res = checker.IsSmooth(quotients, factor_base, ZZ_p(31));

    REQUIRE(false == res);
    }
  }

TEST_CASE("test ecm")
  {
  ECMSmoothChecker checker(to_ZZ(261));
  ZZ_p::init(ZZ(2139274181159L));

  Vec<ZZ_p> factor_base = convert_vector(get_primes(261));

  Vec<ZZ_p> quotients;
  SECTION("48528168585 is smooth bnd 261")
    {
    bool res = checker.IsSmooth(quotients, factor_base, ZZ_p(48528168585));

    REQUIRE(true == res);
    }

  SECTION("898287128670 is smooth bnd 261")
    {
    bool res = checker.IsSmooth(quotients, factor_base, ZZ_p(898287128670));

    REQUIRE(true == res);
    }

  }