cmake_minimum_required(VERSION 3.4)
project(bachelor)

find_package(MPI REQUIRED)

set(BOOST_ROOT "/usr/local/Cellar/boost/1.60.0_2" )

set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)

set(CMAKE_CXX_COMPILE_FLAGS ${CMAKE_CXX_COMPILE_FLAGS} ${MPI_COMPILE_FLAGS})
set(CMAKE_CXX_LINK_FLAGS ${CMAKE_CXX_LINK_FLAGS} ${MPI_LINK_FLAGS})
include_directories(MPI_INCLUDE_PATH)

find_package( Boost COMPONENTS program_options mpi serialization REQUIRED)

set(CMAKE_CXX_STANDARD 11) # adds -std=c++11
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(SOURCE_FILES src/primes.cpp src/primes.h src/common.h tests/catch.hpp
        tests/test.cpp src/Interfaces/ISieve.h src/Impl/AdlemanSieve.cpp
        src/Impl/AdlemanSieve.h src/Interfaces/ILinearSolver.h src/Impl/SimpleNTLSolver.cpp
        src/Impl/SimpleNTLSolver.h src/Interfaces/ISmoothChecker.h
        src/Impl/TrialDivisionSmoothChecker.cpp src/Impl/TrialDivisionSmoothChecker.h
        src/Impl/NondetermLanczosSolver.cpp src/Impl/NondetermLanczosSolver.h
        src/common.cpp src/ntl_boost_serialization.hpp src/MPITaskRunner.cpp
        src/MPITaskRunner.h src/Application.cpp src/Application.h
        src/ZZFactoring.h src/ZZFactoring.cpp
        src/EC_p.h src/EC_p.cpp
        src/Impl/ECMSmoothChecker.cpp src/Impl/ECMSmoothChecker.h)

set(TEST_FILES tests/test.cpp tests/test_primes.cpp tests/test_smooth_checker.cpp tests/test_solver.cpp)

include_directories(/opt/ntl-9.6.4/include)
link_directories(/opt/ntl-9.6.4/lib)

add_library(bachelor_static ${SOURCE_FILES})
target_link_libraries(bachelor_static ntl m gmp ${Boost_LIBRARIES} mpi)

add_executable(bachelor_test ${TEST_FILES})
target_link_libraries(bachelor_test bachelor_static)

add_executable(bachelor_main src/main.cpp)
target_link_libraries(bachelor_main bachelor_static)

